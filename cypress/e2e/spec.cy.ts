/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

describe('First test', () => {
  it('Visits the initial project page', () => {
    cy.visit('/tabs/tab1');
    cy.contains('PEA');
  });
});
