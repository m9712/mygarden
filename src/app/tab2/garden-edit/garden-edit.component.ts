/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import {
  FormControl,
  FormGroup,
  UntypedFormControl,
  Validators
} from '@angular/forms';
import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  AlertController,
  LoadingController,
  ModalController
} from '@ionic/angular';
import { ToastNotificationService } from '../../services/toast-notification.service';
import { EvolutionResponse } from '../garden-canvas/evolution.service';
import { VegetableService } from '../../services/vegetable.service';
import {
  Garden,
  GardenSelected,
  TemplateName
} from '../../shared/garden.model';
import { StorageService } from '../../services/storage.service';
import { environment } from '../../../environments/environment';
import { FabricService } from '../garden-canvas/fabric.service';
import { TourService } from '../../services/tour.service';
import { Vegetable } from '../../shared/vegetable.model';
import { TranslateService } from '@ngx-translate/core';
import { SwiperOptions } from 'swiper/types';

export interface VegetableGardenModel {
  id: string;
  name: string;
  isChecked: boolean;
}

interface Template {
  label: string;
  value: string;
  imagePath: string;
  checked?: boolean;
}

interface GardenForm {
  templateSlide: FormGroup<{
    templateName: FormControl<TemplateName>;
    gardenWidth: FormControl<number>;
    gardenLength: FormControl<number>;
  }>;
  gardenSlide: FormGroup<{
    name: FormControl<string>;
    description: FormControl<string>;
  }>;
}

@Component({
  selector: 'app-garden-edit',
  templateUrl: './garden-edit.component.html',
  styleUrls: ['./garden-edit.component.scss']
})
export class GardenEditComponent implements OnInit, AfterViewInit {
  @Input() editMode = false;
  @Input() gardenID: string; // If the editMode is true, it contains the name of the editing garden
  @ViewChild('swiperEl', { static: true }) swiperEl: ElementRef | undefined;
  gardenForm: FormGroup<GardenForm>;
  forbiddenGardens = [];
  formIsReady = false;
  allVegetablesAreSelected = false;
  vegetableForm: VegetableGardenModel[];
  garden: Garden; // Garden data if in edit mode
  templates: Template[] = [
    {
      label: 'TAB2.editGardenModal.square',
      value: 'square',
      imagePath: 'assets/images/square-garden.svg'
    },
    {
      label: 'TAB2.editGardenModal.row',
      value: 'row',
      imagePath: 'assets/images/row-garden.svg'
    },
    {
      label: 'TAB2.editGardenModal.custom',
      value: 'custom',
      checked: true,
      imagePath: 'assets/images/paintbrush.webp'
    }
  ];

  constructor(
    private tourService: TourService,
    private modalCtrl: ModalController,
    private translate: TranslateService,
    private fabricService: FabricService,
    private storageService: StorageService,
    private loadingCtrl: LoadingController,
    private alertController: AlertController,
    private vegetableService: VegetableService,
    private toastNotificationService: ToastNotificationService
  ) {}

  async ngOnInit() {
    // Get gardens names and add them to the blocklist
    this.storageService.getGardens().then((gardens: Garden[]) => {
      this.forbiddenGardens = gardens === null ? [] : gardens;
      this.forbiddenGardens.push({ name: 'firstTime' });
    });
    if (this.editMode) {
      await this.storageService
        .getGarden(this.gardenID)
        .then((garden: Garden) => {
          this.garden = garden;
          this.formInit();
        });
    } else {
      this.formInit();
    }
  }

  ngAfterViewInit() {
    const swiperParams: SwiperOptions = {
      initialSlide: this.editMode ? 1 : 0,
      allowTouchMove: false
    };
    Object.assign(this.swiperEl?.nativeElement, swiperParams);
    this.swiperEl?.nativeElement.initialize();
  }

  onTemplateClicked() {
    if (this.tourService.isActive()) {
      this.tourService.goTo('goToSetSize');
    }
  }
  forbiddenNames(control: UntypedFormControl): { [s: string]: boolean } {
    // Check if the name is correct or not
    if (this.forbiddenGardens !== undefined && this.forbiddenGardens !== null) {
      if (
        this.forbiddenGardens
          .map(gardenToCheck => {
            return gardenToCheck.name;
          })
          .indexOf(control.value) !== -1
      ) {
        // Convert Object to array
        return { ['nameIsForbidden']: true }; // Name is forbidden
      } else {
        return null; // Name is allowed
      }
    }
    return null;
  }

  onSelectAll() {
    this.allVegetablesAreSelected = !this.allVegetablesAreSelected;
    this.vegetableForm.map((vegetable: VegetableGardenModel) => {
      return this.allVegetablesAreSelected
        ? (vegetable.isChecked = true)
        : (vegetable.isChecked = false);
    });
  }

  onNextSlide() {
    if (this.swiperEl) {
      this.swiperEl.nativeElement.swiper.slideNext();
    }
  }

  getSlideIndex() {
    if (this.swiperEl?.nativeElement?.swiper) {
      return this.swiperEl.nativeElement.swiper.activeIndex;
    }
    return 0; // Return 0 bt default
  }

  onPreviousSlide() {
    if (this.swiperEl) {
      this.swiperEl.nativeElement.swiper.slidePrev();
    }
  }

  isFormValid(): boolean {
    if (this.gardenForm.valid) {
      return true;
    } else if (this.editMode) {
      return this.gardenForm.get('gardenSlide').valid;
    } else {
      if (this.gardenForm.get('gardenSlide').valid) {
        if (
          this.gardenForm.get('templateSlide').value.templateName === 'custom'
        ) {
          return true;
        }
      }
    }
    return false;
  }

  async onTemplateChosen() {
    if (this.tourService.isActive()) {
      this.tourService.goTo('setGardenSize');
    }
    if (
      this.gardenForm.value.templateSlide.templateName === 'row' ||
      this.gardenForm.value.templateSlide.templateName === 'square'
    ) {
      const alert = await this.alertController.create({
        header: this.translate.instant('TAB2.editGardenModal.gardenDimension'),
        inputs: [
          {
            type: 'number',
            name: 'width',
            min: 0,
            placeholder:
              this.translate.instant('TAB2.editGardenModal.width') + ' (m)'
          },
          {
            type: 'number',
            name: 'length',
            min: 0,
            placeholder:
              this.translate.instant('TAB2.editGardenModal.length') + ' (m)'
          }
        ],
        buttons: [
          {
            text: this.translate.instant('GLOBAL.cancel'),
            role: 'test',
            handler: () => !this.tourService.isActive()
          },
          {
            role: 'confirm',
            text: this.translate.instant('GLOBAL.NEXT'),
            handler: dimensions => {
              this.gardenForm
                .get('templateSlide')
                .get('gardenWidth')
                .setValue(dimensions.width);
              this.gardenForm
                .get('templateSlide')
                .get('gardenLength')
                .setValue(dimensions.length);
              if (this.gardenForm.get('templateSlide').valid) {
                this.onNextSlide();
                if (this.tourService.isActive()) {
                  this.tourService.goTo('gardenInfo');
                }
              } else {
                return false;
              }
            }
          }
        ]
      });
      await alert.present();
    } else {
      this.onNextSlide();
    }
  }

  /**
   * Save garden into local storage
   */
  async onSave() {
    // Create a list for selected vegetables
    const vegetablesId: string[] = [];
    const extra: any =
      this.editMode && this.garden.extra ? this.garden.extra : {};
    this.vegetableForm.forEach((vegetable: VegetableGardenModel) => {
      if (vegetable.isChecked === true) {
        vegetablesId.push(vegetable.id);
      }
    });

    extra.plantingMonth =
      this.vegetableService.getVegetablePlantingMonths(vegetablesId);

    const newGarden: Garden = new Garden(
      this.gardenForm.get('gardenSlide').get('name').value,
      this.gardenForm.get('gardenSlide').get('description').value,
      vegetablesId,
      undefined,
      extra
    );

    if (this.isFormValid()) {
      if (this.editMode) {
        await this.storageService.updateGarden(this.gardenID, newGarden);
      } else {
        newGarden.extra.databaseSchemaVersion =
          environment.lastDatabaseSchemaVersion;
        const backgroundGarden: GardenSelected =
          this.storageService.gardenSelected.getValue();
        if (backgroundGarden) {
          // In case user doesn't save the current garden changement.
          await this.fabricService.saveGardenDraw(
            backgroundGarden.garden.name,
            backgroundGarden.date.getFullYear(),
            backgroundGarden.date.getMonth()
          );
        }
        this.storageService.addGarden(newGarden).then(async garden => {
          if (this.gardenForm.value.templateSlide.templateName !== 'custom') {
            const loading = await this.loadingCtrl.create({
              message: this.translate.instant('TAB2.canvas.gardenGeneration')
            });
            await loading.present();
            this.fabricService
              .applyTemplate(
                this.gardenForm.value.templateSlide.templateName,
                this.gardenForm.value.templateSlide.gardenWidth,
                this.gardenForm.value.templateSlide.gardenLength,
                garden.garden,
                garden.date.getFullYear(),
                garden.date.getMonth()
              )
              .then(
                async () => {
                  await loading.dismiss();
                },
                async (error: EvolutionResponse) => {
                  // An error occurred
                  await loading.dismiss();
                  const alert = await this.alertController.create({
                    header: this.translate.instant(
                      'GLOBAL.errors.somethingWrong'
                    ),
                    message: error.message,
                    buttons: ['OK']
                  });
                  await alert.present();
                }
              );
          }
          await this.toastNotificationService.presentToast({
            text: this.translate.instant('GLOBAL.gardenSaved')
          });
        });
      }
    } else {
      console.warn('vegetableForm invalid ! \n', this.gardenForm);
    }
    this.editMode = false;
    await this.dismissModal();
  }

  async dismissModal() {
    this.editMode = false;
    await this.modalCtrl.dismiss();
  }

  private formInit() {
    this.formIsReady = false;
    let name = '';
    let description = '';
    // Vegetables checked uses NgModel
    // Get all Vegetables selected and prepare data to show them in the form
    this.vegetableForm = this.vegetableService
      .getVegetables()
      .map((vegetable: Vegetable) => {
        return {
          name: vegetable.name,
          id: vegetable.id,
          isChecked:
            this.editMode && this.garden.vegetable.includes(vegetable.id)
        };
      });

    if (this.editMode) {
      // Update form from garden data
      name = this.garden.name;
      description = this.garden.description;

      if (
        this.vegetableService.getVegetablesLen() ===
        this.garden.vegetable.length
      ) {
        this.allVegetablesAreSelected = true;
      }
    }
    this.formIsReady = true;

    // Init the vegetable Form
    this.gardenForm = new FormGroup<GardenForm>({
      gardenSlide: new FormGroup({
        name: new FormControl<string>(name, [
          Validators.required,
          this.forbiddenNames.bind(this),
          Validators.maxLength(20),
          Validators.minLength(3)
        ]),
        description: new FormControl<string>(description)
      }),
      templateSlide: new FormGroup({
        templateName: new FormControl<TemplateName>('custom', {
          nonNullable: true
        }),
        gardenWidth: new FormControl(null, [
          Validators.min(0),
          Validators.required
        ]),
        gardenLength: new FormControl(null, [
          Validators.min(0),
          Validators.required
        ])
      })
    });
  }
}
