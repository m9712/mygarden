/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { VegetableService } from '../../services/vegetable.service';
import { Garden, GardenSelected } from '../../shared/garden.model';
import { StorageService } from '../../services/storage.service';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  gardens: Garden[];
  @Input() gardenSelected: GardenSelected;
  currentYear: number = new Date().getFullYear();
  currentMonth: number = new Date().getMonth();

  constructor(
    public modalController: ModalController,
    private storageService: StorageService,
    private vegetableService: VegetableService
  ) {}

  get months() {
    if (
      this.gardenSelected.garden.extra &&
      this.gardenSelected.garden.extra.plantingMonth
    ) {
      return this.vegetableService.convertMonthToString(
        this.gardenSelected.garden.extra.plantingMonth,
        1
      );
    }
  }

  async ngOnInit() {
    this.storageService.gardensUpdated.subscribe((gardens: Garden[]) => {
      this.gardens = gardens;
    });
  }

  onChangeGarden(e) {
    this.storageService.getGarden(e.detail.value).then((garden: Garden) => {
      this.storageService.gardenSelected.next({
        garden,
        date: new Date(
          new Date().getFullYear(),
          garden.extra
            ? garden.extra.plantingMonth.sort((a, b) => a - b)[0]
            : null
        )
      });
    });
  }

  onChangeYear(value) {
    this.storageService.gardenSelected.next({
      ...this.storageService.gardenSelected.value,
      date: new Date(
        new Date(String(value)).getFullYear(),
        this.gardenSelected.date.getMonth()
      )
    });
  }

  onChangeMonth(month: number) {
    this.storageService.gardenSelected.next({
      ...this.storageService.gardenSelected.value,
      ...{
        date: new Date(this.gardenSelected.date.getFullYear(), month)
      }
    });
  }

  getEndTime() {
    return Math.max.apply(null, Object.keys(this.gardenSelected.garden.maps));
  }

  getStartTime() {
    return Math.min.apply(null, Object.keys(this.gardenSelected.garden.maps));
  }
}
