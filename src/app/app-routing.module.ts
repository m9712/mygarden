/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { WelcomeScreenComponent } from './welcome-screen/welcome-screen.component';
import { MigrationComponent } from './migration/migration.component';

import { Guard } from './services/guard.service';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./tabs/tabs.module').then(m => m.TabsPageModule),
    canActivate: [Guard],
    canDeactivate: [!Guard]
  },
  { path: 'welcome-screen', component: WelcomeScreenComponent },
  { path: 'migration', component: MigrationComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
