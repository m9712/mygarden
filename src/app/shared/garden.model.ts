/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

export type TemplateName = 'custom' | 'row' | 'square';

/**
 * Garden model used to store the gardens in the local storage.
 * @author DERACHE Adrien
 */
export class Garden {
  public name: string;
  public description: string;
  public vegetable?: string[];
  public extra?: GardenExtra;
  public maps?: { ['year']: object };

  constructor(
    name: string,
    description: string,
    vegetable?: string[],
    maps?: { ['year']: object },
    extra?: GardenExtra
  ) {
    this.name = name;
    this.description = description;
    this.vegetable = vegetable;
    this.maps = maps;
    this.extra = extra;
  }
}

/**
 * Garden extra information in the local storage
 */
export class GardenExtra {
  public plantingMonth: number[];
  public databaseSchemaVersion: number;
  public templateName?: TemplateName;
  public gardenWidth?: number;
  public gardenLength?: number;

  constructor(
    plantingMonth: number[],
    databaseSchemaVersion: number,
    templateName?: TemplateName,
    gardenWidth?: number,
    gardenLength?: number
  ) {
    this.plantingMonth = plantingMonth;
    this.databaseSchemaVersion = databaseSchemaVersion;
    this.templateName = templateName;
    this.gardenWidth = gardenWidth;
    this.gardenLength = gardenLength;
  }
}

/**
 * Relationship between the vegetables and the zones.
 */
export class VegetableMapping {
  public vegetableId: string;
  public zoneName: string;

  constructor(vegetableId: string, zoneName: string) {
    this.vegetableId = vegetableId;
    this.zoneName = zoneName;
  }
}

/**
 * Garden model emitted in whole app on changes.
 */
export class GardenSelected {
  public garden: Garden;
  public date: Date;

  constructor(garden: Garden, date: Date) {
    this.garden = garden;
    this.date = date;
  }
}
