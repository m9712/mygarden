/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {
  ThemeDetection,
  ThemeDetectionResponse
} from '@awesome-cordova-plugins/theme-detection/ngx';
import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { ScreenOrientation } from '@capacitor/screen-orientation';
import { MigrationService } from './services/migration.service';
import { LanguageService } from './services/language.service';
import { isPlatform, ModalController } from '@ionic/angular';
import { HistoryService } from './services/history.service';
import { LottiePlayer } from '@lottiefiles/lottie-player';
import { StatusBar, Style } from '@capacitor/status-bar';
import { TourService } from './services/tour.service';
import { Router } from '@angular/router';
import { App } from '@capacitor/app';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  lottieAnimationSubscription: Subscription;
  lottieEle: any;

  constructor(
    private languageService: LanguageService,
    private historyService: HistoryService,
    private router: Router,
    private themeDetection: ThemeDetection,
    private modalCtrl: ModalController,
    private tourService: TourService,
    private migrationService: MigrationService
  ) {}

  async ngOnInit() {
    if (isPlatform('mobile')) {
      this.backButtonInit();
      this.themeInit();
      // TODO: Temporary workaround to avoid issues.
      await ScreenOrientation.lock({ orientation: 'portrait' });
    }
    this.languageService.setInitialAppLanguage();

    this.lottieAnimationSubscription =
      this.tourService.lottieAnimation.subscribe((e: boolean) => {
        if (e) {
          this.lottieEle.style.zIndex = 999;
          this.lottieEle.play();
          this.lottieEle.addEventListener('complete', () => {
            this.lottieEle.style.zIndex = -999;
            this.lottieEle.destroy();
          });
        }
      });

    this.migrationService.isUpToDate().then((res: boolean) => {
      if (res) {
        console.log('Debug: Migration is running...');
        this.migrationService.updateDatabaseSchema();
      }
    });
  }

  backButtonInit() {
    App.addListener('backButton', () => {
      if (this.tourService.isActive()) {
        return;
      } // Disable backButton if the user tour is active.
      this.modalCtrl.getTop().then(async modal => {
        if (modal) {
          await this.modalCtrl.dismiss(); // close modal if opened
        } else {
          const precedentTab = this.historyService.pop();
          if (precedentTab) {
            await this.router.navigate([
              'tabs',
              this.historyService.currentTab
            ]);
          } else {
            await App.minimizeApp(); // or minimize app
          }
        }
      });
    });
  }

  themeInit() {
    const setStatusBarStyleDark = async () => {
      await StatusBar.setStyle({ style: Style.Dark });
    };
    const setStatusBarStyleLight = async () => {
      await StatusBar.setStyle({ style: Style.Light });
      await StatusBar.setBackgroundColor({ color: '#ffffff' });
    };

    this.themeDetection
      .isAvailable()
      .then((res: ThemeDetectionResponse) => {
        if (res.value) {
          this.themeDetection
            .isDarkModeEnabled()
            .then((response: ThemeDetectionResponse) => {
              if (response.value) {
                setStatusBarStyleDark().then();
              } else {
                setStatusBarStyleLight().then();
              }
            })
            .catch((error: any) => console.error(error));
        }
      })
      .catch((error: any) => console.error(error));
  }

  ngAfterViewInit() {
    this.lottieEle = document.querySelector('lottie-player') as LottiePlayer;
  }

  ngOnDestroy() {
    this.lottieAnimationSubscription.unsubscribe();
  }
}
