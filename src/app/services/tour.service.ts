/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import { offset } from '@floating-ui/dom';
import { BehaviorSubject } from 'rxjs';
import Shepherd from 'shepherd.js';

@Injectable({ providedIn: 'root' })
export class TourService {
  lottieAnimation = new BehaviorSubject<boolean>(false);
  private tour = new Shepherd.Tour({
    useModalOverlay: true,
    keyboardNavigation: false,
    defaultStepOptions: {
      floatingUIOptions: {
        middleware: [offset(10)]
      },
      scrollTo: false,
      canClickTarget: false,
      classes: 'shepherd'
    }
  });
  private stepHistory: string[] = [];
  private lastStepId: string;

  constructor(private translate: TranslateService) {}

  /**
   * Start User Onboarding.
   */
  start() {
    this.tourInit();
    this.tour.start();
  }

  /**
   * Return tour status
   * @return boolean true:Active; false:Inactive
   */
  isActive(): boolean {
    return this.tour.isActive();
  }

  /**
   * Pause the user tour
   */
  pause() {
    this.lastStepId = this.tour.getCurrentStep().id;
    this.tour.hide();
  }

  /**
   * Resume user tour afeter a pause.
   */
  resume() {
    this.tour.show(this.lastStepId);
  }

  /**
   * Got to a specific step.
   * @param id step ID
   */
  goTo(id: string) {
    if (id === this.lastStepId || this.stepHistory.includes(id)) {
      return;
    }
    this.pause();
    this.tour.show(id);
  }

  /**
   * Hide the tour.
   */
  hide() {
    this.tour.hide();
  }

  /**
   * Go to the next step
   */
  next() {
    this.tour.next();
  }

  /**
   * Stop the tour.
   */
  stop() {
    this.tour.complete();
  }

  /**
   * Init the tour by adding steps.
   * @private
   */
  private tourInit() {
    this.tour.addSteps([
      // TAB 1
      {
        id: 'welcome',
        title: this.translate.instant('INTRO.welcome'),
        text: this.translate.instant('INTRO.tutorialGuide'),
        buttons: [
          {
            text: this.translate.instant('GLOBAL.NEXT'),
            action: this.tour.next
          }
        ],
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      },
      {
        id: 'seasonalVegetable',
        text: this.translate.instant('INTRO.vegetableSeasonal'),
        attachTo: {
          element: '.seasonalVegetableTour',
          on: 'bottom'
        },
        buttons: [
          {
            text: this.translate.instant('GLOBAL.NEXT'),
            action: this.tour.next
          }
        ],
        modalOverlayOpeningRadius: 22,
        modalOverlayOpeningPadding: -2,
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      },
      {
        id: 'allVegetable',
        text: this.translate.instant('INTRO.vegetableAll'),
        attachTo: {
          element: '.allVegetableTour',
          on: 'bottom'
        },
        buttons: [
          {
            text: this.translate.instant('GLOBAL.NEXT'),
            action: this.tour.next
          }
        ],
        modalOverlayOpeningRadius: 20,
        modalOverlayOpeningPadding: -1,
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      },
      {
        id: 'vegetableDetail',
        canClickTarget: true,
        text: this.translate.instant('INTRO.vegetableDetail'),
        attachTo: {
          element: '.vegetableDetailTour',
          on: 'bottom'
        },
        buttons: [
          {
            text: this.translate.instant('GLOBAL.NEXT'),
            action: () => {
              return this.tour.show('forwardOnTab2');
            }
          }
        ],
        modalOverlayOpeningRadius: 25,
        modalOverlayOpeningPadding: 5,
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      },
      // VEGETABLE MODAL !
      {
        id: 'quickInfo',
        text: this.translate.instant('INTRO.vegetableModal.tags'),
        beforeShowPromise: () => {
          return new Promise<void>(resolve => {
            this.waitForElm('.quickInfoTour').then(() => {
              setTimeout(() => {
                resolve();
              }, 1000);
            });
          });
        },
        attachTo: {
          element: '.quickInfoTour',
          on: 'bottom'
        },
        buttons: [
          {
            text: this.translate.instant('GLOBAL.NEXT'),
            action: this.tour.next
          }
        ],
        modalOverlayOpeningRadius: 20,
        modalOverlayOpeningPadding: 2,
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      },
      {
        id: 'badFavAssociation',
        text: this.translate.instant('INTRO.vegetableModal.associations'),
        attachTo: {
          element: '.associationsTour',
          on: 'bottom'
        },
        buttons: [
          {
            text: this.translate.instant('GLOBAL.NEXT'),
            action: this.tour.next
          }
        ],
        modalOverlayOpeningRadius: 20,
        modalOverlayOpeningPadding: 2,
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      },
      {
        id: 'gardenSection',
        canClickTarget: true,
        text: this.translate.instant('INTRO.vegetableModal.gardenSection'),
        attachTo: {
          element: '.gardenSectionTour',
          on: 'bottom'
        },
        modalOverlayOpeningRadius: 5,
        modalOverlayOpeningPadding: 0,
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      },
      {
        id: 'endVegetableModal',
        text: this.translate.instant('INTRO.vegetableModal.endVegetableModal'),
        buttons: [
          {
            text: 'OK',
            action: () => {
              this.pause();
            }
          }
        ],
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      },
      // END VEGETABLE MODAL
      {
        id: 'forwardOnTab2',
        canClickTarget: true,
        text: this.translate.instant('INTRO.gardenCreationTab'),
        attachTo: {
          element: '.forwardTab2Tour',
          on: 'bottom'
        },
        modalOverlayOpeningRadius: 5,
        modalOverlayOpeningPadding: 0,
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      },
      // END TAB1
      // TAB2
      {
        id: 'addGardenButton',
        canClickTarget: true,
        text: this.translate.instant('INTRO.gardenCreation.getStarted'),
        beforeShowPromise: () => {
          return new Promise<void>(resolve => {
            this.waitForElm('.getStartedTour').then(() => {
              setTimeout(() => {
                resolve();
              }, 500);
            });
          });
        },
        attachTo: {
          element: '.getStartedTour',
          on: 'bottom'
        },
        modalOverlayOpeningRadius: 5,
        modalOverlayOpeningPadding: 5,
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      },
      // Edit garden Form
      {
        id: 'gardenTemplate',
        canClickTarget: false,
        text: this.translate.instant('INTRO.gardenCreation.gardenTemplate'),
        beforeShowPromise: () => {
          return new Promise<void>(resolve => {
            this.waitForElm('.garden-tour-template').then(() => {
              setTimeout(() => {
                resolve();
              }, 1000);
            });
          });
        },
        attachTo: {
          element: '.garden-tour-template',
          on: 'bottom'
        },
        buttons: [
          {
            text: this.translate.instant('GLOBAL.NEXT'),
            action: this.tour.next
          }
        ],
        modalOverlayOpeningRadius: 5,
        modalOverlayOpeningPadding: 5,
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      },
      {
        id: 'squareTemplate',
        canClickTarget: true,
        text: this.translate.instant('INTRO.gardenCreation.chooseTemplate'),
        attachTo: {
          element: '.squareTemplate',
          on: 'bottom'
        },
        modalOverlayOpeningRadius: 5,
        modalOverlayOpeningPadding: 5,
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      },
      {
        id: 'goToSetSize',
        canClickTarget: true,
        text: this.translate.instant('INTRO.gardenCreation.goToSetSize'),
        attachTo: {
          element: '.go-to-garden-size-tour',
          on: 'top'
        },
        modalOverlayOpeningRadius: 5,
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      },
      {
        id: 'setGardenSize',
        canClickTarget: false,
        text: this.translate.instant('INTRO.gardenCreation.setSize'),
        buttons: [
          {
            text: this.translate.instant('GLOBAL.NEXT'),
            action: () => {
              this.pause();
            }
          }
        ],
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      },
      {
        id: 'gardenInfo',
        canClickTarget: true,
        text: this.translate.instant('INTRO.gardenCreation.gardenInfo'),
        beforeShowPromise: () => {
          return new Promise<void>(resolve => {
            this.waitForElm('.gardenInfoTour').then(() => {
              setTimeout(() => {
                resolve();
              }, 1000);
            });
          });
        },
        attachTo: {
          element: '.gardenInfoTour',
          on: 'bottom'
        },
        buttons: [
          {
            text: this.translate.instant('GLOBAL.NEXT'),
            action: this.tour.next
          }
        ],
        modalOverlayOpeningRadius: 5,
        modalOverlayOpeningPadding: 5,
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      },
      {
        id: 'vegetableSelect',
        canClickTarget: true,
        text: this.translate.instant('INTRO.gardenCreation.vegetableSelection'),
        attachTo: {
          element: '.vegetableSelectTour'
        },
        buttons: [
          {
            text: this.translate.instant('GLOBAL.NEXT'),
            action: () => {
              this.pause();
            }
          }
        ],
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      },
      // END garden Form
      {
        id: 'yearSelect',
        canClickTarget: false,
        text: this.translate.instant('INTRO.gardenDrawing.years'),
        attachTo: {
          element: '.yearTour',
          on: 'bottom'
        },
        buttons: [
          {
            text: this.translate.instant('GLOBAL.NEXT'),
            action: this.tour.next
          }
        ],
        modalOverlayOpeningRadius: 10,
        modalOverlayOpeningPadding: 5,
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      },
      {
        id: 'endTour',
        canClickTarget: false,
        text: this.translate.instant('INTRO.gardenDrawing.endOfTour'),
        buttons: [
          {
            text: this.translate.instant('GLOBAL.END'),
            action: () => {
              this.lottieAnimation.next(true);
              this.stop();
            }
          }
        ],
        modalOverlayOpeningRadius: 10,
        modalOverlayOpeningPadding: 5,
        when: {
          show: () => {
            this.stepHistory.push(this.tour.getCurrentStep().id);
          }
        }
      }
      // END TAB2
    ]);
  }

  /**
   * Resolve the promise when element appear in the DOM.
   * @param selector get the element by class.
   * @private
   */
  private waitForElm(selector) {
    return new Promise(resolve => {
      if (document.querySelector(selector)) {
        return resolve(document.querySelector(selector));
      }

      const observer = new MutationObserver(mutations => {
        if (document.querySelector(selector)) {
          resolve(document.querySelector(selector));
          observer.disconnect();
        }
      });

      observer.observe(document.body, {
        childList: true,
        subtree: true
      });
    });
  }
}
