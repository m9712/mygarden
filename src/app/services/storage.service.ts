/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { Garden, GardenExtra, GardenSelected } from '../shared/garden.model';
import { Storage } from '@ionic/storage-angular';
import { BehaviorSubject, Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
/**
 * Garden manager service
 * @link https://github.com/ionic-team/ionic-storage
 * @link https://m9712.gitlab.io/vegetables-db/Getting%20started/Basic_Structure/#database-schema
 * @author DERACHE Adrien
 */
export class StorageService {
  gardensUpdated = new BehaviorSubject<Garden[]>(null);
  gardenSelected = new BehaviorSubject<GardenSelected>(null);
  languageSelected = new Subject<string>();

  constructor(private storage: Storage) {
    this.createDataBase().then();
  }

  /**
   * Return true if the app is launched for the first time.
   */
  async isFirstLaunch(): Promise<boolean> {
    return await this.storage.get('firstTime').then(async res => {
      if (res === null || res === 'true') {
        return true;
      } else if (res === 'false') {
        return false;
      }
    });
  }

  /**
   * Set firstTime to false in local storage.
   */
  async unLockApp() {
    await this.storage.set('firstTime', 'false');
  }

  /**
   * Outputs the gardens
   * @return Garden[]
   */
  async getGardens(): Promise<Garden[]> {
    return new Promise<Garden[]>(async resolve => {
      const gardens: Garden[] = [];
      const storageLength = await this.storage.length();
      await this.storage.forEach((value, key, index) => {
        let garden: Garden | null;
        try {
          garden = JSON.parse(value).name ? JSON.parse(value) : null; // Should fail if value is not of type Garden
        } catch (error) {
          garden = null;
        }
        if (garden !== null) {
          gardens.push({
            name: garden.name,
            description: garden.description,
            vegetable: garden.vegetable,
            maps: garden.maps,
            extra: garden.extra
          });
        }
        if (storageLength === index) {
          if (gardens.length <= 0) {
            resolve(null);
          }
          resolve(gardens);
        }
      });
    });
  }

  /**
   * Update the app language.
   * Do not use it directly but use setLanguage() of LanguageService instead.
   * @param language The new language.
   */
  async updateLanguage(language: string) {
    await this.storage.set('languageSelected', language);
  }

  /**
   * Get the language used.
   * @return {string} two language letter, Ex: fr, en.
   */
  async getLanguageSelected() {
    return await this.storage.get('languageSelected');
  }

  /**
   * Get specific garden
   * @param name the garden id
   * @return Garden
   */
  async getGarden(name: string): Promise<Garden> {
    return new Promise<Garden>(async resolve => {
      const garden = JSON.parse(await this.storage.get(name));
      resolve(garden);
    });
  }

  /**
   * Add a garden into local storage
   * @param garden Garden object to save
   */
  async addGarden(garden: Garden) {
    const name: string = garden.name;
    const extra: GardenExtra = garden.extra;
    const vegetable: string[] = garden.vegetable;
    const description: string = garden.description;

    function generateMapObject() {
      // Generate an array from the current year to current year + 25 years
      const array = [...Array(25).keys()]
        .slice(1)
        .map(x => x + new Date().getFullYear() - 1);
      const object = new Object({});
      for (const item of array) {
        object[item] = {};
      }
      return object;
    }

    const maps = garden.maps ?? generateMapObject();
    console.log(maps);
    return this.storage
      .set(name, JSON.stringify({ name, description, vegetable, maps, extra }))
      .then(async (resGarden: string) => {
        const newGarden: GardenSelected = {
          garden: JSON.parse(resGarden),
          date: new Date(
            new Date().getFullYear(),
            Math.min(...garden.extra.plantingMonth) === Infinity
              ? null
              : Math.min(...garden.extra.plantingMonth)
          )
        };
        this.gardensUpdated.next(await this.getGardens());
        this.gardenSelected.next(newGarden);
        console.info('Garden: ' + name + ' saved !');
        return newGarden;
      });
  }

  /**
   * Updates a specific map in a garden with the given name, year, and month.
   * @param name The name of the garden to update.
   * @param map The map to update, as a JSON string.
   * @param year The year of the map to update.
   * @param month The month of the map to update (0-11).
   */
  async updateMap(name: string, map: string, year: number, month: number) {
    const gardenMap = JSON.parse(map);
    return this.getGarden(name).then(async (garden: Garden) => {
      garden.maps[year][month] = gardenMap;
      await this.storage.set(name, JSON.stringify(garden));
      return garden;
    });
  }

  /**
   * Updates the `maps` property of a garden with the given `name` using the
   * `gardenMaps` object. The updated garden is then stored in local storage.
   * @param name The name of the garden to update.
   * @param maps A JSON string representing the updated `maps` object.
   * @returns A promise that resolves when the garden has been successfully updated
   * and stored in local storage.
   */
  async updateMaps(name: string, maps: string) {
    const gardenMaps = JSON.parse(maps);
    await this.getGarden(name).then(async (garden: Garden) => {
      garden.maps = gardenMaps;
      await this.storage.set(name, JSON.stringify(garden));
    });
  }

  /**
   * Update a garden NOT THE MAP but keep it !.
   * @param oldGardenName old garden ID
   * @param newGarden new garden object
   */
  async updateGarden(oldGardenName: string, newGarden: Garden) {
    // Remove the old garden and create new with new properties
    // Get the map if exists
    if (!('maps' in newGarden) || !newGarden.maps) {
      await this.getGarden(oldGardenName).then((garden: Garden) => {
        if ('maps' in garden) {
          newGarden.maps = garden.maps;
        }
      });
    }
    return this.deleteGarden(oldGardenName).then(() => {
      return this.addGarden(newGarden);
    });
  }

  /**
   * Delete all the gardens
   */
  async deleteGardens() {
    await this.storage.clear();
    this.gardensUpdated.next(await this.getGardens());
    this.gardenSelected.next(null);
    // tslint:disable-next-line:no-console
    console.info('Gardens deleted !');
  }

  /**
   * Delete specific garden
   * @param name garden ID
   */
  async deleteGarden(name: string) {
    await this.storage.remove(name).then(
      async () => {
        this.gardensUpdated.next(await this.getGardens());
        this.getGardens().then(gardens => {
          if (gardens === null) {
            this.gardenSelected.next(null);
          } else {
            this.getGarden(gardens[0].name).then((garden: Garden) => {
              this.gardenSelected.next({
                garden,
                date: new Date(
                  new Date().getFullYear(),
                  Math.min(...garden.extra.plantingMonth) === Infinity
                    ? null
                    : Math.min(...garden.extra.plantingMonth)
                )
              });
            });
          }
        });
        console.info('Garden: ' + name + ' deleted');
      },
      error => {
        console.error(error);
      }
    );
  }

  /**
   * Init database and dispatch gardens in whole app with rxjs Subject.
   * @private
   */
  private async createDataBase() {
    this.storage.create().then(
      () => {
        console.log('Storage successfully created !');
      },
      error => {
        console.error(error);
      }
    );
    // Dispatch gardens
    this.getGardens().then((gardens: Garden[]) => {
      if (gardens != null) {
        this.gardensUpdated.next(gardens);
        this.gardenSelected.next({
          garden: gardens[0],
          date: new Date(
            new Date().getFullYear(),
            gardens[0].extra
              ? Math.min(...gardens[0].extra.plantingMonth)
              : null
          )
        });
      }
    });
  }
}
