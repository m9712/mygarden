/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TourService } from '../../../services/tour.service';
import { Vegetable } from '../../../shared/vegetable.model';

@Component({
  selector: 'app-vegetable-item',
  templateUrl: './vegetable-item.component.html',
  styleUrls: ['./vegetable-item.component.scss']
})
export class VegetableItemComponent {
  @Input() vegetable: Vegetable;
  @Output() vegetableSelected = new EventEmitter<void>();

  constructor(private tourService: TourService) {}

  onSelected() {
    if (this.tourService.isActive()) {
      this.tourService.goTo('quickInfo');
    }
    this.vegetableSelected.emit();
  }
}
