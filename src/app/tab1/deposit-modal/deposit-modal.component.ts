/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Vegetable } from '../../shared/vegetable.model';
import { TourService } from '../../services/tour.service';

@Component({
  selector: 'app-deposit-modal',
  templateUrl: './deposit-modal.component.html',
  styleUrls: ['./deposit-modal.component.scss']
})
export class DepositModalComponent {
  @Input() vegetable: Vegetable;
  segmentValue = 'ID'; // Default segment
  seasonString = [];

  constructor(
    private modalCtrl: ModalController,
    private tourService: TourService
  ) {}

  onChangeVegetable(event: Vegetable) {
    this.vegetable = event;
  }

  onSegmentChanged() {
    if (this.segmentValue === 'garden' && this.tourService.isActive()) {
      this.tourService.goTo('endVegetableModal');
    }
  }

  dismissModal() {
    if (this.tourService.isActive()) {
      this.tourService.goTo('forwardOnTab2');
    }
    this.modalCtrl.dismiss().then();
  }
}
