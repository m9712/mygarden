/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { LanguageService } from '../services/language.service';
import { StorageService } from '../services/storage.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Component implements OnInit {
  selectedLanguage: string;

  constructor(
    private storageService: StorageService,
    private languageService: LanguageService
  ) {}

  ngOnInit() {
    this.storageService.getLanguageSelected().then(lang => {
      this.selectedLanguage = lang;
    });
  }

  onSwitchLanguage(language) {
    this.languageService.setLanguage(language.detail.value);
  }
}
