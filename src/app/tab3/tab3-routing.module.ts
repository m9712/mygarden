/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { RouterModule, Routes } from '@angular/router';
import { Tab3Component } from './tab3.page';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: Tab3Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab3PageRoutingModule {}
