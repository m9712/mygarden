/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

export const environment = {
  production: true,
  lastDatabaseSchemaVersion: 2.0
};
